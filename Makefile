all:
	xelatex -synctex=1 -shell-escape -interaction=nonstopmode xeCJK.tex
clean:
	rm *.aux *.log *.gz
